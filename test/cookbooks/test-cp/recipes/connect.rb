# frozen_string_literal: true

#
# Copyright (c) 2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Create a file to use as source for FileSource Connector
file '/tmp/source-test.txt' do
  content <<-FILE.gsub(/^ */, '')
    1 aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
    2 bbbbbbbbbbbbbbb
    3 ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
    4 dddddddddddddddddddddddddddd
    5 e
  FILE
end
